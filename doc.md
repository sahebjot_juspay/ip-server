## deu command line (v1.0.0)

### install
 - {installCommandHere}

### commands
 - createApp or ca
   > creates app with a random name unless specified and opens in editor
   ## example
   * `deu ca my-app-name`
   * `deu ca`
   * `deu createApp my-app-name`
 - list or ls
   > list's all created app
   ## example
   * `deu list`
 - start <app-name>
   > start's existing app
   ## example
   * `deu start my-already-created-app`
 - delete <app-name> or rm <app-name>
   > delete existing app
   * `deu rm my-already-created-app`
