const express = require('express')
const app = express()
const port = 3000
const fs = require('fs')
const marked = require('marked')
const U = require('./utils')
const https = require('https')
const generateInstall = require('./generateInstall')
const async = require('asyncawait/async')
const await = require('asyncawait/await')

let db = {

}

let cache = {

}

const bitbucketCommitsUrl = 'https://bitbucket.org/sahebjot_juspay/deu/commits/branch/master'

app.get('/', (req, res) => {
  let key = req.query.key
  let data = req.query.data

  db[key] = data

  res.send(db)
})

app.get('/get', (req, res) => {
  res.send(db[req.query.key])
})

const getCommitData = () => {
  return new Promise((resolve, reject) => {
    let rawData = ''
    https.get(bitbucketCommitsUrl, (htmlStream) => {
        htmlStream.on('data', (chunk) => {
          rawData += chunk
        })
        htmlStream.on('end', () => {
          resolve(rawData)
        })
        htmlStream.on('error', (e) => {
          reject(e)
        })
      })
  })
}

const getLatestCommit = (rawHtml) => {
  return rawHtml.split("chg_2").shift().split("hash execute").pop().split("href=").pop().split("commits/").pop().split('?at').shift()
}

const fetchFromCacheOrLatest = async((currentTime) => {
  if(cache['install']) {
    let timeDiff = Math.abs(currentTime - cache['install'].time)
    let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))
    if(diffDays > 1) { // if more than a day then update the cache
      return updateCache(await(fetchLatestData()), currentTime)
    } else {
      return cache['install'].data
    }
  } else {
    return updateCache(await(fetchLatestData()), currentTime) // stores in cache first time
  }
})

const fetchLatestData = async(() => {
  let commitData = await(getCommitData())
  let latestCommit = getLatestCommit(commitData)
  return generateInstall(latestCommit)
})

const updateCache = (cacheData, currentTime) => {
  cache['install'] = {
    'data': cacheData,
    'time': currentTime
  }
  return cacheData
}

app.get('/updateCache', async((req, res) => {
  res.json(updateCache(await(fetchLatestData()), (new Date()).getTime()))
}))

app.get('/doc', async((req, res) => {
  let data = await(fetchFromCacheOrLatest((new Date()).getTime()))
  let allMd = fs.readFileSync('./doc.md').toString("utf8").replace("{installCommandHere}", data)
  let file = marked(allMd)
  res.send(file)
}))

app.get('/getfile', (req, res) => {
  const key = req.query.key
    try {
      const userData = JSON.parse(db[key]);
    } catch(e) {
      res.send({"error": true, "message": ("error while parsing json for key "+ key)})
    }
  res.send(U.getFile("test", "temp"))
})

app.get('/cleardb', (req, res) => {
  db = {}
  res.send({"error": false, "message": "success"})
})

app.listen(port, () => {
  console.log("listening on port", port)
})
