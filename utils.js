const fs = require('fs')
const config = require('./config')

exports.getFile = (user, file) => {
  return fs.readFileSync(config.filePath + '/' + user + '/' + file)
}

exports.writeFile = (user, file) => {
  return fs.writeFileSync(config.filePath + '/' + user + '/' + file)
}
