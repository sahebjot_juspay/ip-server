module.exports = (gitLatestHash) => {
  const installUrl = `https://bitbucket.org/sahebjot_juspay/deu/raw/${gitLatestHash}/dist/deu-mac`
  return `curl -# ${installUrl} -o /usr/local/bin/deu && chmod a+x /usr/local/bin/deu`
}
